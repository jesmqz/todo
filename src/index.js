
// import { Todo } from './classes/todo.class';
// import { TodoList } from './classes/todo-list.class';

import { Todo, TodoList } from './classes';
import { crearTodoHtml } from './js/componentes';

import './styles.css';


export const todoList = new TodoList();
// const tarea1 = new Todo( 'Learn JS!!');
// const tarea2 = new Todo( 'Learn AWS');

// todoList.nuevoTodo(tarea1);
// todoList.nuevoTodo(tarea2);
console.info( todoList);

// tarea1.completado = false;
// crearTodoHtml( tarea1 );

// todoList.todos.forEach(todo => crearTodoHtml(todo) );
todoList.todos.forEach( crearTodoHtml );

// console.log(tarea);

// const lastTodo = new Todo('Buy course');
// todoList.nuevoTodo(lastTodo);

// todoList.todos[0].imprimirClase();

// console.log('todos', todoList.todos);
