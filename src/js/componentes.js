import '../css/componentes.css';
// import webpacklogo from '../assets/img/webpack-logo.png';
import { Todo } from '../classes';
import { todoList } from '../index';

const divTodoList     = document.querySelector('.todo-list');
const txtInput        = document.querySelector('.new-todo');
const btnBorrar       = document.querySelector('.clear-completed');
const ulFilter        = document.querySelector('.filters');
const anchorFiltros   = document.querySelectorAll('.filtro');

export const saludar = ( nombre ) => {

    console.log('creando etiqueta h1');

    const h1 = document.createElement('h1');

    h1.innerHTML = `Hola, ${ nombre }`;

    document.body.append( h1 );

    // console.log(webpacklogo);
    // una forma de cargar la imagen
    // const img = document.createElement('img');
    // img.src = webpacklogo;
    // document.body.append(img);

}

export const crearTodoHtml = ( todo ) => {
    const htmlTodo = `
    <li class="${ (todo.completado) ? 'completed' : '' }" data-id="${ todo.id }">
    <div class="view">
        <input class="toggle" type="checkbox" ${ (todo.completado) ? 'checked' : '' }>
        <label>${ todo.tarea }</label>
        <button class="destroy"></button>
    </div>
    <input class="edit" value="Create a TodoMVC template">
    </li>`;

    const div = document.createElement('div');
    div.innerHTML = htmlTodo;
    divTodoList.append( div.firstElementChild );
    return div.firstElementChild;


}

txtInput.addEventListener('keyup', ( event ) => {

    if (event.keyCode === 13) {
        console.log(txtInput.value);
        const nuevoTodo = new Todo(txtInput.value);

        todoList.nuevoTodo( nuevoTodo );

        console.log(todoList);

        crearTodoHtml(nuevoTodo);

        txtInput.value = '';
    }
    // console.log(event);

});

divTodoList.addEventListener('click', ( event ) => {

    console.log(event.target.localName);
    // console.log('click');
    const nombreElemento = event.target.localName;
    const todoElemento = event.target.parentElement.parentElement;
    const todoId = todoElemento.getAttribute('data-id');

    if (nombreElemento.includes('input')) {
        todoList.marcarCompletado( todoId );
        todoElemento.classList.toggle('completed');
        console.log(todoList);
    } else if(nombreElemento.includes('button')) {
        todoList.eliminarTodo(todoId);
        divTodoList.removeChild( todoElemento );
        console.log(todoList);
    }

    // console.log(todoId);

});

btnBorrar.addEventListener('click', () => {
    console.log('eliminar completados');

    for(let i = 0; i < divTodoList.children.length; i++) {

        console.log(divTodoList.children[i]);
        const elemento = divTodoList.children[i];

        if (elemento.classList.contains('completed')) {
            divTodoList.removeChild(elemento);

        }

    }
    // console.log(divTodoList.children);

});

ulFilter.addEventListener('click', ( event ) => {
    const filtro = event.target.text;

    if (!filtro) { return };

    anchorFiltros.forEach(element => element.classList.remove('selected'));

    console.log( 'target', event.target );
    event.target.classList.add('selected');
    

    for (const elemento of divTodoList.children ) {
        elemento.classList.remove('hidden');
        const completado = elemento.classList.contains('completed');

        switch( filtro ) {
            case 'Pendientes':
                if (completado) {
                    elemento.classList.add('hidden');
                }
            break;

            case 'Completados':
                if (!completado) {
                    elemento.classList.add('hidden');
                }
            break;
        }
    }

});
